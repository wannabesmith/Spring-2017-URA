\documentclass[]{article}
\usepackage[cm]{fullpage}
\usepackage{lmodern,amsmath,amssymb}
\usepackage{a4wide}
\usepackage{amsthm}
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage{cite}
\usepackage{amsmath}
\usepackage{bm}

\newtheorem{theorem}{Theorem}[section]
\newtheorem{proposition}{Proposition}[theorem]
\newtheorem{lemma}{Lemma}[theorem]
\DeclareMathOperator{\tr}{tr}
%opening
\title{Some t-test equivalents for $d=2$}
\author{}

\begin{document}

\maketitle

\begin{proposition}\label{prop1}
	If $T \sim T(\nu)$ then $T^2 \sim F(1, \nu)$
	
	\begin{proof}
		
		Suppose $T = \frac{Z}{\sqrt{X/\nu}}$ such that $X \sim \chi^2(\nu)$ and $Z \sim N(0,1)$. Then we have,
		\begin{align}
			T^2 &= \frac{Z^2}{X/\nu} \\
			&= \frac{Z^2/1}{X/\nu} \sim F(1, \nu)
		\end{align}
		
	\end{proof}
\end{proposition}

\section{Equal variances}

Let $$s_p = \sqrt{\frac{(n_1 - 1)s_{X_1}^2 + (n_2 - 1)s_{X_2}^2}{n_1 + n_2 - 2}}$$

the unbiased estimator for the pooled standard deviation of both samples. 
\\

Let $$t = \frac{\bar{x}_1 - \bar{x}_2}{s_p\sqrt{\frac{1}{n_1} + \frac{1}{n_2}}}$$

the $t$-statistic used to test $H_{0,1}: \mu_1 = \mu_2$ under the assumption that $\sigma_1 = \sigma_2$ 
\\

Let $$F = \frac{SSB / (d-1)}{SSW / (n - d)}$$

the $F$-statistic used to test $H_{0,2}: \mu_1 = \mu_2 = \cdots = \mu_d$

\begin{proposition}\label{prop2}
	If $d = 2$, then $t^2 = F$

\begin{proof}
	We first write $$t^2 = \frac{(\bar x_1 - \bar x_2)^2}{s_p^2(\frac{1}{n_1} + \frac{1}{n_2})}$$
	
	When $d = 2$, we have $$F = \frac{SSB}{SSW / (n - 2)}$$
	
	Therefore, it suffices to show that 
	
	$$SSW/(n-2) = s_p^2$$
	
	and
	
	$$SSB = \frac{(\bar x_1 - \bar x_2)^2}{\frac{1}{n_1} +
		\frac{1}{n_2}}$$ 
	
	
	Firstly, 
	\begin{align*}
	\frac{SSW}{n-2} &= \frac{(\sum x_{1i} - \bar x_1) + \sum (x_{2i} - \bar x_2)}{n - 2} \\ 
	&= \frac{(n_1 - 1)s_{X_1}^2 + (n_2 - 1)s_{X_2}^2}{n_1 + n_2 - 2} \\ 
	&= s_p^2
	\end{align*}
	
	Now, 
	\begin{align*}
	SSB &= n_1(\bar x_1 - \bar x)^2 + n_2(\bar x_2 - \bar x) \\
	&= n_1 \bar x_1^2 - 2n_1 \bar x \bar x_1 - 2n_2 \bar x \bar x_2 + n_2 \bar x_2^2 + (n_1 + n_2)\bar x^2 \\
	&= n_1 \bar x_1^2 + \bar x(-2n_1 \bar x_1 - 2n_2 \bar x_2 + (n_1 + n_2)\bar x) + n_2 \bar x_2^2 \\
	&= n_1 \bar x_1^2 + \bar x \Big (-2n_1\bar x_1 -2n_2\bar x_2 + (n_1 + n_2)\frac{n_1 \bar x_1 + n_2 \bar x_2}{n_1 + n_2} \Big ) + n_2 \bar x_2^2 \\
	&= n_1 \bar x_1^2 + \bar x(-n_1 \bar x_1 - n_2 \bar x_2) + n_2 \bar x_2^2 \\
	&= n_1 \bar x_1^2 - \frac{(n_1 \bar x_1 + n_2 \bar x_2)^2}{n_1 + n_2} + n_2 \bar x_2^2 \\
	&= n_1 \bar x_1^2 - \frac{n_1^2\bar x_1^2 + n_2^2 \bar x_2^2 + 2n_1\bar x_1 n_2 \bar x_2}{n_1 + n_2} + n_2 \bar x_2^2 \\
	&= \frac{(n_1^2 + n_1n_2)\bar x_1^2 -n_1^2 \bar x_1^2 +(n_1n_2 + n_2^2)\bar x_2^2 - n_2^2 \bar x_2^2 - 2n_1\bar x_1 n_2 \bar x_2}{n_1 + n_2} \\
	&= \frac{n_1n_2}{n_1 + n_2}(\bar x_1^2 - 2\bar x_1 \bar x_2 + \bar x_2^2) \\
	&= \frac{(\bar x_1 - \bar x_2)^2}{\frac{1}{n_1} + \frac{1}{n_2}}
	\end{align*}
\end{proof}

\end{proposition}

\begin{lemma}
	Let $p_F$ and $p_T$ be the p-values obtained by testing $H_{0,1}$ and $H_{0,2}$ respectively. Then $p_F = p_T$.
	
	\begin{proof}
		\begin{align*}
			p_F &= \Pr(F > \hat F) \\ 
			&= \Pr(T^2 > \hat t^2) \\
			&= \Pr(|T| > \hat t) \\
			&= p_T
		\end{align*}
		
		This follows from propositions \ref{prop1} and \ref{prop2}
	\end{proof}
\end{lemma}

\section{Unequal variances}

\subsection{The Wald-type statistic (WTS)}

Let $$Q = Q_n(T) = n{\bf \bar X'T(T\hat V_n T)^+T\bar X}$$

the so-called Wald-type statistic (WTS) as described by Pauly et al. \cite{pauly}.
\\

Let $$t = \frac{\bar{x}_1 - \bar{x}_2}{\sqrt{\frac{s_1^2}{n_1} + \frac{s_2^2}{n_2}}}$$

the $t$-statistic used to test $H_{0,3}:\mu_1 = \mu_2$ when it is \textit{not} assumed that $\sigma_1 = \sigma_2$

\begin{proposition}\label{prop3}
	If $d = 2$, then $t^2 = Q$ 
\end{proposition}

\begin{proof}
	Firstly, since $d = 2$, we have that 
	${\bf H} = 
	\begin{pmatrix}
		1 & -1
	\end{pmatrix}
	$.
	Therefore, 
	$${\bf T = H'(HH')^-H} =
	\frac{1}{2}
	\begin{pmatrix}
	1 & -1 \\
	-1 & 1
	\end{pmatrix}$$
	
	Now, consider ${\bf \hat V_n}$ as described by Pauly et al. \cite{pauly}
	
	$$
	{\bf \hat V_n} 
	= \bigoplus_{i=1}^d \frac{n}{n_i} \hat \sigma_i^2 \\
	=
	n
	\begin{pmatrix}
	\frac{s_1^2}{n_1} & 0 \\
	0 & \frac{s_2^2}{n_2}
	\end{pmatrix}
	$$
	
	Let $a = \frac{s_1^2}{n_1}$ and $b = \frac{s_2^2}{n_2}$. Then we have that 
	\begin{align*}
	{\bf T\hat V_n T} &=
	\frac{n}{4}
	\begin{pmatrix}
	1 & -1 \\
	-1 & 1
	\end{pmatrix}
	\begin{pmatrix}
	a & 0 \\
	0 & b
	\end{pmatrix}
	\begin{pmatrix}
	1 & -1 \\
	-1 & 1
	\end{pmatrix} \\
	&= 
	\frac{n}{4}
	\begin{pmatrix}
		a+b & -a-b \\
		-a-b & a+b
	\end{pmatrix}
	\end{align*}
	
	Since ${\bf T \hat V_n T}$ is a singular matrix, we compute the Moore-Penrose pseudoinverse denoted by $(\cdot)^+$
	
	$$({\bf T \hat V_n T})^+ 
	= \frac{1}{an + bn}
	\begin{pmatrix}
	1 & -1 \\
	-1 & 1
	\end{pmatrix}$$
	
	Therefore, we have 
	\begin{align*}
		Q &= n{\bf \bar X'T(T\hat V_n T)^+T\bar X} \\
		&= \frac{n}{4(an + bn)}
		\begin{pmatrix}
			\bar x_1 & \bar x_2
		\end{pmatrix}
		\begin{pmatrix}
			1 & -1 \\
			-1 & 1
		\end{pmatrix}^3
		\begin{pmatrix}
			\bar x_1 \\
			\bar x_2 
		\end{pmatrix} \\
		&= \frac{1}{4(a + b)}
		\begin{pmatrix}
		\bar x_1 & \bar x_2
		\end{pmatrix}
		\begin{pmatrix}
		4 & -4 \\
		-4 & 4
		\end{pmatrix}
		\begin{pmatrix}
		\bar x_1 \\
		\bar x_2 
		\end{pmatrix} \\
		&= \frac{1}{(a + b)}
		(\bar x_1^2 -2 \bar x_1 \bar x_2 + \bar x_2^2) \\
		&= \frac{(\bar x_1 - \bar x_2)^2}
		{\frac{s_1^2}{n_1} + \frac{s_2^2}{n_2}} \\
		&= t^2
	\end{align*}
\end{proof}

\subsection{The ANOVA-type statistic (ATS)}

Let $$F = F_n(T) = \frac{n {\bf \bar X'T\bar X}}{\tr({\bf T \hat V_n})}$$

The so-called ANOVA-type statistic (ATS) as described by Pauly et al.\cite{pauly} whose null distribution is approximated by an $F$-distribution with 

$$f_1 = \frac{\tr({\bf T \hat V_n})^2}{\tr\{({\bf T \hat V_n})^2\}}$$

$$f_2 = \frac{\tr(\mathbf{T} \mathbf{\hat V_n})^2}{\tr (\mathbf{D_T}^2 \mathbf{\hat V_n}^2 \mathbf{\Lambda})}$$

\begin{proposition}\label{prop4}
	$t^2 = F = Q$
	
	\begin{proof}
		Let $a = \frac{s_1^2}{n_1}$ and $b = \frac{s_2^2}{n_2}$. Then
		
		\begin{align*}
			F = F_N(T) &= \frac{n \mathbf{\bar X'T\bar X}}{\tr(\mathbf{T \hat V_n})} \\
			&= \frac{n \mathbf{\bar X'}
				\begin{pmatrix}
					\frac{1}{2} & -\frac{1}{2} \\
					-\frac{1}{2} & \frac{1}{2}
				\end{pmatrix} \mathbf{\bar X}}
			{\tr
				\begin{pmatrix}
					\frac{an}{2} & -\frac{bn}{2} \\
					-\frac{an}{2} & \frac{bn}{2}
				\end{pmatrix}
				} \\
			&= 
			\frac{
				\begin{pmatrix}
					\bar x_1 & \bar x_2
				\end{pmatrix}
				\begin{pmatrix}
					1 & -1 \\
					-1 & 1
				\end{pmatrix}
				\begin{pmatrix}
					\bar x_1\\
					\bar x_2
				\end{pmatrix}
			}
			{a + b} \\
			&= \frac{(\bar x_1 - \bar x_2)^2}{\frac{s_1^2}{n_1} + \frac{s_2^2}{n_2}} \\
			&= t^2
		\end{align*}
		
		It follows from proposition \ref{prop3} that $t^2 = F = Q$
	\end{proof}
\end{proposition}

\begin{proposition}\label{prop5}
	Let $F(f_1, f_2)$ be the null distribution of $F_n(T)$ and let $T(\nu)$ be the null distribution of $t$. If $X \sim T(\nu)$ then $X^2 \sim F(f_1, f_2)$. 
	
	\begin{proof}
		By proposition \ref{prop1}, it suffices to prove that $f_1 = 1$ and $f_2 = \nu$.
		
		First we will show that $f_1 = 1$.
		
		\begin{align*}
			f_1 &= \frac{\tr({\bf T \hat V_n})^2}{\tr\{({\bf T \hat V_n})^2\}} \\
			&= \frac{(\frac{an + bn}{2})^2}{\frac{a^2n^2}{4} + \frac{b^2n^2}{4} + \frac{abn^2}{2}} \\
			&= 1
		\end{align*}
		
		Now, we will show that $f_2 = \nu$. 
		
		\begin{align*}
			f_2 &= \frac{\tr(\mathbf{T} \mathbf{\hat V_n})^2}{\tr (\mathbf{D_T}^2 \mathbf{\hat V_n}^2 \mathbf{\Lambda})} \\
			&= \frac{(\frac{an + bn}{2})^2}{\frac{1}{4}(\frac{a^2n^2}{n_1 -1} + \frac{b^2n^2}{n_2 - 1})} \\
			&= \frac{\Big(\frac{s_1^2}{n_1} + \frac{s_2^2}{n_2}\Big)^2}{\frac{s_1^4}{n_1(n_1 - 1)} + \frac{s_2^4}{n_2(n_2-1)}} \\
			&= \nu
		\end{align*}
	\end{proof}
\end{proposition}

\begin{lemma}
	Let $p_T$ be the p-value obtained using Welch's $t$-test and let $p_F$ be the p-value obtained using $F_n(T) \sim F(f_1, f_2)$. Then $p_T = p_F$.
	
	\begin{proof}
		This follows immediately from propositions \ref{prop4} and \ref{prop5}.
	\end{proof}
\end{lemma}

\begin{proposition}
	Let $F_n \sim F(1, \nu)$ where $\nu$ is as in Welch's $t$-test. Then $F_n \xrightarrow{\mathcal D} X \sim \chi^2(1)$.
	
	\begin{proof}
		$F_n = \frac{A/1}{B/\nu}$ for some $A \sim \chi^2(1), B\sim \chi^2(\nu)$.
		
		Note that as $n \rightarrow \infty$, we have $\nu \rightarrow \infty$.
		
		By the central limit theorem, $$B \xrightarrow{\mathcal D} N(\nu, 2\nu)$$ and so $$B/\nu \xrightarrow{\mathcal D} N(1, \frac{2}{\nu})$$
		$$\implies B/\nu \xrightarrow{\mathcal D} N(1, 0)$$
		$$\implies B/\nu \xrightarrow{\mathcal P} 1$$
		
		Therefore, by Slutksy's theorem, 
		
		$$F_n = \frac{A/1}{B/\nu} \xrightarrow{\mathcal D} A \sim \chi^2(1)$$
	\end{proof}
\end{proposition}

\begin{thebibliography}{2}
	\bibitem{pauly} Asymptotic permutation tests in general factorial designs. 
\end{thebibliography}

\end{document}
